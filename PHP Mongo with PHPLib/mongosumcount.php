<?php

include "mongokoneksi.php";

//======Sum with match======

$pipeline = [
    [
        '$match' => [
            'nama_kapal' => 'Kapal A'
        ]
    ],
    [
        '$group' => [
            '_id' => [
                'nama_kapal' => '$nama_kapal',
		
            ],
            'total' => [
                '$sum' => '$tahun'
            ]
        ]
    ],
   [
	'$project' =>[
		'total' => '$total',
		'nama_kapal' => '$_id.nama_kapal'
	]
    ], 
    [
        '$sort' => [
            '_id' => -1
        ]
    ],
    [
        '$limit' => 100
    ]
];

$options=[];

$cursor = $collection->aggregate($pipeline, $options);

foreach ($cursor as $document) {
    echo "Select sum(tahun) as total where kapal=".$document['nama_kapal']." group by Kapal result is ".$document['total']."<br>";
}

echo "<hr>";

//======Sum group by no filter======

$pipeline = [
    [
        '$group' => [
            '_id' => [
                'nama_kapal' => '$nama_kapal',
		
            ],
            'total' => [
                '$sum' => '$tahun'
            ]
        ]
    ],
    [
	'$project' =>[
		'total' => '$total',
		'nama_kapal' => '$_id.nama_kapal'
	]
    ],
    [
        '$sort' => [
            '_id' => -1
        ]
    ],
    [
        '$limit' => 100
    ]
];

$options=[];

$cursor = $collection->aggregate($pipeline, $options);

foreach ($cursor as $document) {
    echo "Select sum(tahun) as total group by Kapal  ".$document['nama_kapal']." result is ".$document['total']."<br>";
}

echo "<hr>";


//======Count All======

$pipeline = [
     [
        '$group' => [
            '_id' => [
                'nama_kapal' => '$nama_kapal',
		
            ],
            'total' => [
                '$sum' => 1
            ]
        ]
    ],
   [
	'$project' =>[
		'total' => '$total',
		'nama_kapal' => '$_id.nama_kapal'
	]
    ], 
    [
        '$sort' => [
            '_id' => -1
        ]
    ],
    [
        '$limit' => 100
    ]
];

$options=[];

$cursor = $collection->aggregate($pipeline, $options);

foreach ($cursor as $document) {
    echo "Select count(tahun) as total group by Kapal  ".$document['nama_kapal']." result is ".$document['total']."<br>";
}

echo "<hr>";


//======Count with match======

$pipeline = [
    [
        '$match' => [
           'berlaku' => [ '$gte' => '2010-01-01','$lte' => '2010-03-03' ] 
        ]
    ],
    [
        '$group' => [
            '_id' => [
                'nama_kapal' => '$nama_kapal',
		
            ],
            'total' => [
                '$sum' => 1
            ]
        ]
    ],
   [
	'$project' =>[
		'total' => '$total',
		'nama_kapal' => '$_id.nama_kapal'
	]
    ], 
    [
        '$sort' => [
            '_id' => -1
        ]
    ],
    [
        '$limit' => 100
    ]
];

$options=[];

$cursor = $collection->aggregate($pipeline, $options);

foreach ($cursor as $document) {
    echo "Select count(tahun) as total where kapal=".$document['nama_kapal']." group by Kapal result is ".$document['total']."<br>";
}

echo "<hr>";

//======Sum with date======

$pipeline = [
    [
        '$match' => [
           'berlaku' => [ '$gte' => '2010-01-01','$lte' => '2010-03-03' ] 
        ]
    ],
    [
        '$group' => [
            '_id' => [
                'nama_kapal' => '$nama_kapal',
		
            ],
            'total' => [
                '$sum' => '$tahun'
            ]
        ]
    ],
   [
	'$project' =>[
		'total' => '$total',
		'nama_kapal' => '$_id.nama_kapal'
	]
    ], 
    [
        '$sort' => [
            '_id' => -1
        ]
    ],
    [
        '$limit' => 100
    ]
];

$options=[];

$cursor = $collection->aggregate($pipeline, $options);

foreach ($cursor as $document) {
    echo "Select sum(tahun) as total where tanggal btween '2010-01-01' and '2010-03-02' group by Kapal result is ".$document['total']."<br>";
}


?>